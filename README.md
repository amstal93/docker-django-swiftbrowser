# Django SwiftBrowser Docker Image

## References

*	[Django SwiftBrowser](https://github.com/cschwede/django-swiftbrowser)
*	[Swift Browser UI](https://github.com/CSCfi/swift-browser-ui)

## Build

### The Latest Version by Docker

~~~sh
docker build --pull -t "registry.gitlab.com/rychly-edu/docker/docker-django-swiftbrowser:latest" .
~~~

### All Versions by the Build Script

~~~sh
./build.sh --build "registry.gitlab.com/rychly-edu/docker/docker-django-swiftbrowser" "latest"
~~~

For the list of versions to build see [docker-tags.txt file](docker-tags.txt).
